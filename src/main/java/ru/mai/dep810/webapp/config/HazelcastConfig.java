package ru.mai.dep810.webapp.config;

import com.hazelcast.config.Config;
import com.hazelcast.config.EvictionPolicy;
import com.hazelcast.config.MapConfig;
import com.hazelcast.config.MaxSizeConfig;
import com.hazelcast.config.QueueConfig;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.mai.dep810.webapp.cache.Caches;

@Configuration
public class HazelcastConfig {

    @Bean(destroyMethod = "shutdown")
    public HazelcastInstance createStorageNode(@Qualifier("StorageNodeConfig") Config config) {
        return Hazelcast.newHazelcastInstance(config);
    }

    @Bean(name = "StorageNodeConfig")
    public Config config() {
        Config config = new Config();
        config.setProperty("hazelcast.jmx", "true");
//        config.setProperty("hazelcast.partition.count", "5");

        // Queue configuration
        QueueConfig emailQueueConfig = new QueueConfig();
        emailQueueConfig.setName("email-queue");
        config.addQueueConfig(emailQueueConfig);

        // Users cache configuration
        MapConfig userMapConfig = new MapConfig(Caches.USER.name());
        userMapConfig.setBackupCount(0);
        userMapConfig.setAsyncBackupCount(0);
        userMapConfig.setMaxSizeConfig(new MaxSizeConfig()
                .setMaxSizePolicy(MaxSizeConfig.MaxSizePolicy.PER_NODE)
                .setSize(2000));
        userMapConfig.setTimeToLiveSeconds(60);
        userMapConfig.setEvictionPolicy(EvictionPolicy.LRU);

        config.addMapConfig(userMapConfig);

        return config;
    }

}
